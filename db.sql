n SQL Dump
-- version 3.4.5deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2012 at 06:48 AM
-- Server version: 5.1.63
-- PHP Version: 5.3.6-13ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `notetime`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notelist_id` int(10) unsigned NOT NULL,
  `name` varchar(80) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `notelist_id_2` (`notelist_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=571 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `notelist_id`, `name`, `description`) VALUES
(554, 19, 'a\r', ''),
(555, 19, 'b\r', ''),
(556, 19, 'c', ''),
(560, 19, '1\r', ''),
(562, 22, 'Aloe \r', ''),
(563, 22, 'Pledge  \r', ''),
(564, 22, 'Pork rinds\r', ''),
(565, 22, 'Chip beef \r', ''),
(566, 22, 'Cheetos \r', ''),
(567, 22, 'Bread \r', ''),
(568, 22, 'Lunch meat\r', ''),
(569, 22, 'Buttermilk (half cup)\r', ''),
(570, 22, 'Parsley', '');

-- --------------------------------------------------------

--
-- Table structure for table `notelists`
--

CREATE TABLE IF NOT EXISTS `notelists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL COMMENT 'Owner of notelist',
  `name` varchar(60) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `notelists`
--

INSERT INTO `notelists` (`id`, `user_id`, `name`, `created`, `modified`) VALUES
(19, 5, 'List1', '2012-09-24 04:20:48', '2012-09-24 04:20:48'),
(21, 4, 'My List', '2012-09-24 06:13:10', '2012-09-24 06:13:10'),
(22, 6, 'Groceries', '2012-09-24 06:22:13', '2012-09-24 06:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`, `is_active`, `created`, `modified`) VALUES
(4, 'winston', '48a6e66c24f3c349cf98c12d2c51dda60d5e9a7d', 'notetaker', 1, '2012-09-18 06:50:02', '2012-09-24 06:13:10'),
(5, 'john', '48a6e66c24f3c349cf98c12d2c51dda60d5e9a7d', 'notetaker', 1, '2012-09-18 06:50:44', '2012-09-24 04:20:48'),
(6, 'brandy', '48a6e66c24f3c349cf98c12d2c51dda60d5e9a7d', 'notetaker', 1, '2012-09-24 00:00:00', '2012-09-24 06:22:13');

-- --------------------------------------------------------

--
-- Table structure for table `user_notelists`
--

CREATE TABLE IF NOT EXISTS `user_notelists` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `notelist_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='Join table for notelists and users.' AUTO_INCREMENT=24 ;

--
-- Dumping data for table `user_notelists`
--

INSERT INTO `user_notelists` (`id`, `notelist_id`, `user_id`) VALUES
(18, 19, 5),
(21, 19, 4),
(22, 21, 4),
(23, 22, 6);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

