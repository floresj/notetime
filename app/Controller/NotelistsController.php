<?php
class NotelistsController extends AppController {
	
	public function isAuthorized($user){
		return true;
		if(parent::isAuthorized($user)){
			// Index page
			if($this->action === 'index'){
				return true;
			} else if ($this->action === 'edit'){
				$notelistId = $this->request->params['pass'][0];
				return $this->Notelist->isAuthorized($user['id'], $notelistId);
			}
			//return $this->Notelist->isAuthorized($userId,$notelistId);
		} else {
			return false;
		}
	}
	
    public function index(){
        $userId = $this->Auth->user('id');
        $lists = $this->Notelist->getAll($userId);
        $modifiedList = array();
        $this->set('lists',$lists);
    }
    
    public function add(){
        if ($this->request->is('post')) {
			$this->request->data['Notelist']['user_id'] = $this->Auth->user('id');
			$this->request->data['User']['id'] = $this->Auth->user('id');
            if ($this->Notelist->saveAll($this->request->data)) {
                //$this->Session->setFlash('Your post has been saved.');
                $id = $this->Notelist->getInsertId();
                $this->redirect(array('controller'=>'notelists','action' => 'index'));
            } else {
                $this->Session->setFlash('Unable to add your post.');
            }
        }		
	}
	
	public function update($id = null){	
		$this->Notelist->save($this->request->data);
		$this->Session->setFlash('::Updated List Title');
		$this->redirect(array('controller'=>'items', 'action'=>'view/'. $id));	
	}
		
	public function edit($id = null){	
		$this->Notelist->id = $id;
		$this->request->data = $this->Notelist->read();
        $isOwner = false;
        if($this->request->data['Notelist']['user_id'] == $this->Auth->user('id')){
            $isOwner = true;
        }
        $this->set('isOwner', $isOwner);
		$this->set('notelistId', $id);
	}
	
	public function clear($id = null){
		$this->loadModel('Item');
		$this->Item->deleteAll(array('Item.notelist_id'=>$id),false);
		$this->redirect(array('controller'=>'items', 'action'=>'view/' . $id));
	}
	
	public function delete($id = null){
		// Delete cascades and removes Items and HABTM row
        $this->Notelist->deleteAll(array('Notelist.id'=>$id),true);
        $this->redirect(array('controller'=>'userNotelists', 'action'=>'index/'));		

        // Delete child items
		//$this->loadModel('Item');
		//$this->Item->deleteAll(array('Item.notelist_id'=>$id),false);
		// Delete List
		//$this->loadModel('UserNotelist');
		//$this->UserNotelist->deleteAll(array('UserNotelist.notelist_id'=>$id),false);
	}
	
}

?>
