<?php
class ItemsController extends AppController {

    public function add($id){
        $this->set('id', $id);
        if ($this->request->is('post')) {
			$newItems = trim($this->request->data['Item']['name']);
			// Split up the contents of the text area
            $itemArr = explode("\n", $newItems);
            // Clean up any spaces for each element
			$itemArr = array_filter($itemArr, 'trim');
            // Loop through all items and create new Item object
			foreach($itemArr as $itemName){
				$this->Item->create();
                $data = array('name'=>$itemName, 'notelist_id'=>$id);
                $this->Item->save($data);
			}
            $this->request->data['Item']['notelist_id'] = $id;
            $this->redirect(array('controller'=>'items','action' => 'view/' . $id));
            /*
            if ($this->Item->save($this->request->data)) {
                $this->redirect(array('controller'=>'items','action' => 'view/' . $id));
            } else {
                $this->Session->setFlash('Unable to add Item');
            }
            */
        }		
    
    }

    public function view($id = null){
        $userId = $this->Auth->user('id');
        $this->loadModel('Notelist');
        $list = $this->Notelist->find('first', array('recursive' => -1, 'fields' => array('Notelist.name','Notelist.user_id'), 'conditions' => array('Notelist.id' => $id)));
        $this->loadModel('UserNotelist');
        $readOnly = $this->UserNotelist->isReadOnly($id, $userId);
        
        $items = $this->Item->getAll($id);
        $isOwner = false;
        if($list['Notelist']['user_id'] == $this->Auth->user('id')){
            $isOwner = true;
        }
        $this->set('isReadOnly', $readOnly);
        $this->set('isOwner', $isOwner);
        $this->set('notelistName', $list['Notelist']['name']);
        $this->set('notelistId', $id);
        $this->set('items', $items);
    }

    public function sync($id = null){
        $this->layout = '';
        $items = $this->Item->getSync($id);
        $liCount = 0;
        
        // Generate HTML to inject into Items page
        $html = '';
        foreach($items as $item){
            $html .='<li itemcount="'.$liCount.'" itemid="'.$item['Item']['id'].'" class="viewitem-row" data-theme="c" id="item'.$liCount.'"><a href="#"><h3 class="ui-li-heading">'.$item['Item']['name'].'</h3><a href="javascript:Items.remove('.$item['Item']['id'].','.$liCount.');" itemid="'.$item['Item']['id'].'" data-shadow="false" data-iconshadow="true" data-icon="false" data-iconpos="notext" data-theme="a"></a></li>';
            $liCount++;
        }
        if($html != ''){
			$html = '<ul id="items-listview" data-role="listview" data-inset="false" data-split-theme="d" data-split-icon="check" data-split-icon="delete">' . $html . '</ul>';
		}

        // Response
        $response = array();
        $response['success'] = true;
        $response['data'] = $html;

        echo json_encode($response);
        $this->render('/Elements/json');

    }

    public function remove($id = null){
        $this->layout = '';
        $deleted = $this->Item->delete($id);
        if($deleted){
            echo 'true';
        } else {
            echo 'false';
        }
        $this->render('/Elements/json');
    }
}

?>
