<?php
App::uses('AuthComponent', 'Controller/Component');
class UsersController extends AppController {
    public $helpers = array('Html', 'Form', 'Session');
    public $components = array('Session');
    public function beforeFilter() {
        parent::beforeFilter();
    }
    public function login() {
        if($this->Auth->user('id')){
            $this->redirect($this->Auth->redirect());
        }
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect($this->Auth->redirect());
            } else {
                $this->set('errorMsg', 'Invalid username or password');
//                $this->Session->setFlash('Invalid username or password, try again');
            }
        }
    }

    public function logout() {
        $this->redirect($this->Auth->logout());
    }

    public function add() {
        die('Nope, not allowed');
        if ($this->request->is('post')) {
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                $this->Session->setFlash(__('The user has been saved'));
                return $this->redirect(array('controller'=>'lists', 'action' => 'index'));
            } else {
                $this->Session->setFlash(__('The user could not be saved. Please, try again.'));
            }
        }
    }    
}
?>
