<?php
class UserNotelistsController extends AppController {
	
	public function isAuthorized($user){
		return true;
		if(parent::isAuthorized($user)){
			// Index page
			if($this->action === 'index'){
				return true;
			} else if ($this->action === 'edit'){
				$notelistId = $this->request->params['pass'][0];
				return $this->Notelist->isAuthorized($user['id'], $notelistId);
			}
			//return $this->Notelist->isAuthorized($userId,$notelistId);
		} else {
			return false;
		}
	}
	
    public function index(){
        $lists = $this->UserNotelist->getAll($this->Auth->user('id'));
        $this->set('userId', $this->Auth->user('id'));
        $shared = array();
        foreach($lists as $list){
            if($list['UserNotelist']['is_owner'] == 0){
                $userId = $list['Notelist']['user_id'];
                $this->loadModel('User');
                $list['Owner'] = $this->User->getUsernameById($userId);
                array_push($shared, $list);
            }
        }
        $this->set('shared', $shared);
        $this->set('lists', $lists);
        $this->set('username', $this->Auth->user('username'));
    }
    
    public function add(){
        if ($this->request->is('post')) {
            $userId = $this->Auth->user('id');
			// Associate record to primary user
            $this->request->data['User']['id'] = $userId;
            // Assign Owner
            $this->request->data['Notelist']['user_id'] = $userId;
            $this->request->data['UserNotelist']['is_owner'] = true;
			if ($this->UserNotelist->saveAssociated($this->request->data)) {
                $this->redirect(array('action' => 'index'));
            }
        }		
	}
    public function addshare($id = null){
        if($this->request->is('post')){
           if($this->UserNotelist->save($this->request->data)){
                $this->redirect(array('controller'=>'UserNotelists', 'action'=>'share/' . $id));
           }
        } else {
            $this->set('notelistId', $id);
            $this->loadModel('User');
            $this->loadModel('Notelist');
            $this->Notelist->id = $id;
            $listName = $this->Notelist->field('name');
            $userList = $this->User->getUserSharedList($id);
            $shared = $this->UserNotelist->getCurrentlyShared($this->Auth->user('id'), $id);
            $this->set('userList', $userList);
            $this->set('shared', $shared);
            $this->set('listName', $listName);
        }
    }

    public function share($id = null){
        if($this->request->is('post')){

           if($this->UserNotelist->save($this->request->data)){
                $this->redirect(array('controller'=>'items', 'action'=>'view/' . $id));
           }
        } else {
            $this->set('notelistId', $id);
            $this->loadModel('User');
            $this->loadModel('Notelist');
            $this->Notelist->id = $id;
            $listName = $this->Notelist->field('name');
            $userList = $this->User->getUserSharedList($id);
            $shared = $this->UserNotelist->getCurrentlyShared($this->Auth->user('id'), $id);
            $this->set('userList', $userList);
            $this->set('shared', $shared);
            $this->set('listName', $listName);
        }
    }
    public function unshare($userNotelistId = null, $notelistId = null){
       $this->UserNotelist->delete($userNotelistId, false);
       $this->redirect(array('controller'=>'UserNotelists', 'action'=>'share/'. $notelistId ));
    }
}

?>
