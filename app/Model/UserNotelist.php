<?php

class UserNotelist extends AppModel {
	public $actsAs = array('Containable');  
    public $belongsTo = array(
        'User', 'Notelist'
    );
    
    public function isReadOnly($notelistId, $userId){
        $readOnly = $this->find('first', array(
            'conditions'=>array('UserNotelist.user_id'=>$userId, 'UserNotelist.notelist_id'=>$notelistId),
            'contain'=>'',
            'fields'=>array('readonly')
        ));
        $isReadOnly = $readOnly['UserNotelist']['readonly'];
        return $isReadOnly;

    }

    public function getAll($userId){
		$modLists = array();
		$lists = $this->find('all', array(
			'conditions'=>array('UserNotelist.user_id'=>$userId),
			'contain' => array('Notelist')
		));
		
		foreach($lists as $list){
			$id = $list['Notelist']['id'];
			$list['Notelist']['item_count'] = $this->Notelist->Item->getCount($id);;
			if($list['UserNotelist']['is_owner'] == 0){
                //$owner;
            }
            array_push($modLists, $list);
		}
		return $modLists;
	}

    public function getCurrentlyShared($userId, $notelistId){
        $shared = $this->find('all', array(
            'conditions'=>array('UserNotelist.notelist_id'=>$notelistId,
            'UserNotelist.user_id !=' => $userId),
            'fields'=>array('id','readonly','created'),
            'contain'=>array('User'=>array('fields'=>array('id','username')))
        ));
        return $shared;
    }
}
?>
