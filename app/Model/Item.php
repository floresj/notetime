<?php

class Item extends AppModel {
    public $name = 'Item';
    public $actsAs = array('Containable');
    
    public function getSyncIds($listId){
        $items = $this->find('list', array(
            'conditions'=>array('notelist_id'=>$listId),
            'fields'=>array('id')

        ));
        return $items;
    }

    public function getSync($listId){
        $items = $this->find('all', array(
            'conditions'=>array('notelist_id'=>$listId),
            'fields'=>array('id','name')

        ));
        return $items;
    }

    public function getAll($listId){
        $items = $this->find('all', array(
            'conditions'=>array('notelist_id'=>$listId)
        ));
        return $items;
    }

    public function getCount($listId){
		$count = $this->find('count', array(
        'conditions' => array('Item.notelist_id' => $listId)
		));
		return $count;
	}

}

?>
