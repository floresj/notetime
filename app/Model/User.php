<?php

class User extends AppModel {
    public $name = 'User';
    public $actsAs = array('Containable');
    //public $hasMany = array('Notelist');
	public $hasMany = array('UserNotelist');
    
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        )
    );

public function beforeSave($options = array()) {
    if (isset($this->data[$this->alias]['password'])) {
        $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
    }
    return true;
}

public function getUsernameById($userId){
    $this->id = $userId;
    $username = $this->field('username');
    return $username;
}

public function getUserSharedList($notelistId){
    $notelistId = Sanitize::paranoid($notelistId);
    $sql = 'SELECT User.id, User.username FROM users as User,user_notelists where User.id NOT IN (SELECT user_id FROM user_notelists WHERE notelist_id='.$notelistId.') AND User.is_active = 1';
    $users = $this->query($sql);
    $userList = array();
    foreach($users as $user){
        $userList[$user['User']['id']] = $user['User']['username'];
    }
    return $userList;
}

}
?>

