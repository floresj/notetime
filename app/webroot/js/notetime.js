
function change(url){
	$.mobile.changePage(
			url,
			{
			  transition              : 'slide',
			  showLoadMsg             : true,
			  reloadPage              : true
			}
		  );	
}

function status(msg){
	if(msg != undefined && msg != ''){
		$('#status').text(msg);
	}
	$('#statusmessage').fadeIn('slow').trigger( 'updatelayout' );
	setTimeout(function(){
		$('#statusmessage').fadeOut('slow');
	}, 3000);
}

var Items = {
    // Remove item from list
    remove : function(id, elemId){
        $.ajax({
            url: rootUrl +'items/remove'+ '/' + id,
            success: function(data){
                $('#item' + elemId).fadeOut('slow', function(){
						$(this).remove();
				});
                $('#items-listview').listview('refresh');
            },
            error: function(jqXHR, status, error){
                alert('ERROR: ' + error);
            }
        });
    },
    sync: function(){
		$.mobile.changePage(
			window.location.href,
			{
			  allowSamePageTransition : true,
			  transition              : 'none',
			  showLoadMsg             : true,
			  reloadPage              : true
			}
		  );
		/*
        $.ajax({
            dataType:'json',
            type: 'GET',
            url: rootUrl +'items/sync/' + id,
            success: function(result){
                if(result.success == true){
					$('#items-listview').remove();
					$('#listview-container').html('');
					$('#listview-container').append(result.data);
					$('#items-listview').listview();
					status("List synchronized");
                } 
            },
            error: function(jqXHR, status, error){
                alert('ERROR: ' + error);
            }
        });
        * */
    }
}
