<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'index'
    ));
?>
<div data-role="page">
	<div data-theme="a" data-role="header" data-position="fixed">
		<a data-iconpos="notext" data-role="button" data-transition="slidedown" href="<?=$returnUrl;?>" data-icon="back" class="ui-btn-left"></a>
		<h3>
			New List
		</h3>
	</div>
	<div data-role="content" style="padding: 15px">
		<?=$this->Form->create('Notelist');?>		
		<?php
			echo $this->Form->input('name', array('label'=>'List Name'));
		?>
		<?php
			$options = array(
				'label' => 'Add',
				'data-theme'=>'a'
			);		
		?>
		<?=$this->Form->end($options);?>				
	</div>
</div>
