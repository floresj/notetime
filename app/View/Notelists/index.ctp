<?php
    $newListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'add'
    ));
    $viewItemUrl = $this->Html->url(array(
        'controller' => 'items',
        'action'     => 'view'
    ));
    $pageUrl = $this->Html->url(array(
        'controller' => 'notelists',
        'action'     => 'index'
    ));    

?>
<div data-role="page" data-url="<?=$pageUrl;?>">
            <div data-theme="a" data-role="header" data-position="fixed">
                <h3>
                    Note Time
                </h3>
                <a data-role="button" data-mini="true" data-transition="slide" 
                href="<?=$newListUrl;?>"
                data-icon="plus" data-iconpos="left" class="ui-btn-right">
                    New List
                </a>
            </div>
            <div data-role="content" style="padding: 15px">
                <ul id="note-list" data-role="listview" data-divider-theme="b"  data-inset="false">
                    <li data-role="list-divider" role="heading">
                        List of Items
                    </li>
                    <?php foreach($lists as $list): ?>
						<li data-theme="c">
							<a  href="<?=$viewItemUrl . '/'.$list['Notelist']['id'];?>" data-transition="slide">
								<?=$list['Notelist']['name'];?>
								<span class="ui-li-count">
									<?=$list['Notelist']['item_count'];?>
								</span>
							</a>
						</li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
