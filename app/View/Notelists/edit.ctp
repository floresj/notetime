<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'view',
        $notelistId
    ));
    $clearListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'clear',
         $notelistId
    ));    
    $deleteListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'delete',
         $notelistId
    ));     
    $pageUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'edit',
         $notelistId
    ));    
?>
<div data-role="page" data-url="<?=$pageUrl;?>">
	<div data-theme="a" data-role="header" data-position="fixed">
		<a data-iconpos="notext" data-role="button" data-transition="slidedown" href="<?=$returnUrl;?>" data-icon="back" class="ui-btn-left"></a>
		<h3>
			Edit List
		</h3>
	</div>
	<div data-role="content" style="padding: 15px">
		<?=$this->Form->create('Notelist', array('action'=>'update'));?>		
        <?php
			echo $this->Form->hidden('id', array('value'=>$notelistId));
			echo $this->Form->input('name', array('label'=>'Rename List'));
			echo $this->Form->hidden('edited', array('value'=>'1'));
		?>
		<?php
			$options = array(
				'label' => 'Rename',
				'data-theme'=>'a',
				'name'=>'editsubmit'
			);		
		?>
		<?=$this->Form->end($options);?>
		
        <a data-role="button" data-transition="fade" data-theme="b" 
        href="<?=$clearListUrl;?>">
            Clear List
        </a>
        <a data-role="button" data-transition="fade" data-theme="b" href="<?=$deleteListUrl;?>">
            Delete List
        </a>			
	</div>
</div>
<script>

	function change(url){
		$.mobile.changePage(
				url,
				{
				  transition              : 'slide',
				  showLoadMsg             : true,
				  reloadPage              : true
				}
			  );	
	}
</script>
