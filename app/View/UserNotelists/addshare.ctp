
<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'view',
        $notelistId
    ));
    $clearListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'clear',
         $notelistId
    ));    
    $deleteListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'delete',
         $notelistId
    ));     
    $editUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'edit',
         $notelistId
    ));    
    $shareUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'share',
         $notelistId
    ));
    $addShareUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'addshare',
         $notelistId
    ));
    $unshareUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'unshare'
    ));

    
?>
<style>
.ui-select .ui-btn select{
font-size: 50px;
}
</style>
<div data-role="page" data-url="<?=$addShareUrl;?>">
	<div data-theme="a" data-role="header" data-position="fixed">
		<a data-iconpos="notext" data-role="button" data-transition="slidedown" href="<?=$shareUrl;?>" data-icon="back" class="ui-btn-left"></a>
		<h3>
			Share List
		</h3>
    </div>
	<div data-role="content" style="padding: 15px">
    
		<?=$this->Form->create('UserNotelist');?>		
        <?php
			echo $this->Form->hidden('notelist_id', array('value'=>$notelistId));
            echo $this->Form->input('user_id', array(
                'options'=>$userList,
                'label'=>'Select user to share the list <span style="font-weight:bold;text-decoration:underline;">'.$listName.'</span>',
                'data-theme'=>'b',
                'empty'=>'Select User',
                'data-mini'=>true,
                'data-prevent-focus-zoom'=>true

            ));
            echo '<div>Check the box below to prevent users from editing your list. Otherwise, users will be allowed to add and remove items.</div>';
            echo $this->Form->input('readonly', array('label'=>'Read Only?'));
		?>
		<?php
			$options = array(
				'label' => 'Share List',
				'data-theme'=>'b',
				'name'=>'editsubmit'
			);		
		?>
		<?=$this->Form->end($options);?>
	</div>
</div>
