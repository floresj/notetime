
<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'view',
        $notelistId
    ));
    $clearListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'clear',
         $notelistId
    ));    
    $deleteListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'delete',
         $notelistId
    ));     
    $editUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'edit',
         $notelistId
    ));    
    $shareUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'share',
         $notelistId
    ));
    $unshareUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'unshare'
    ));
    $addShareUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'addshare',
         $notelistId
    ));
    
?>

<div data-role="page" data-url="<?=$shareUrl;?>">
	<div data-theme="a" data-role="header" data-position="fixed">
		<a data-iconpos="notext" data-role="button" data-transition="slidedown" href="<?=$returnUrl;?>" data-icon="back" class="ui-btn-left"></a>
		<h3>
			Share List
		</h3>
        <a data-role="button" data-mini="true" data-transition="slide" href="javascript:change('<?=$addShareUrl;?>');" data-icon="plus"  data-iconpos="notext" class="ui-btn-right"></a>                
	</div>
	<div data-role="content" style="padding: 15px">
    
       <ul id="items-listview" data-role="listview" data-icon="check" data-inset="false" data-split-theme="d">
        <li data-role="list-divider" role="heading">
            Users sharing <span style="font-weight:bold;text-decoration:underline;"><?=$listName;?></span>
        </li>
        <?php
            if(count($shared) == 0){
                echo 'This list is not shared with anyone. Click on the add button to share this other users';
            }
        ?>
        <?php foreach($shared as $item): ?>
            <?php
            $unshareUrl = $unshareUrl . '/'. $item['UserNotelist']['id'] . '/' . $notelistId;
            ?>
            <li  data-icon="delete" class="viewitem-row" data-theme="c">
                <a href="javascript:;">
                    <h3 class="ui-li-heading"><?=$item['User']['username'];?>
                    
                    <div style="font-size:10px;">
                        <?php
                            echo 'Permissions: ';
                            if($item['UserNotelist']['readonly'] == true){
                                echo '<span style="text-decoration:underline;font-weight:bold;">Readonly</span>';
                            } else {
                                echo 'Add/Remove Items';
                            }
                            echo '<div>Shared Date <span style="font-weight:bold;text-decoration:underline;">' . $item['UserNotelist']['created'] . '</span></div>';
                        ?>
                    </div>
                    </h3>
                    <a href="<?=$unshareUrl;?>" data-shadow="false" data-iconshadow="true" data-icon="false" data-iconpos="notext" data-theme="a">						
                </a>
            </li>
            <?php endforeach?>
       </ul>
	</div>
</div>
