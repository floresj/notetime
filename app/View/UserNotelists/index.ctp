<?php
    $newListUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'add'
    ));
    $viewItemUrl = $this->Html->url(array(
        'controller' => 'items',
        'action'     => 'view'
    ));
    $pageUrl = $this->Html->url(array(
        'controller' => 'UserNotelists',
        'action'     => 'index'
    ));    
    $logoutUrl = $this->Html->url(array(
        'controller' => 'Users',
        'action'     => 'logout'
    ));    

?>
<div data-role="page" data-url="<?=$pageUrl;?>">
            <div data-theme="a" data-role="header" data-position="fixed">
                <a data-role="button" data-mini="true" data-transition="slide" href="javascript:Items.sync();" data-icon="refresh"  data-iconpos="notext" class="ui-btn-left"></a>                
                <h3>
                    Note Time
                </h3>
                <a data-role="button" data-mini="true" data-transition="slide" href="<?=$newListUrl;?>" data-icon="plus"  data-iconpos="notext" class="ui-btn-right"></a>                
            </div>            
            <div data-role="content" style="padding: 15px">
                <ul style="" id="note-list" data-role="listview" data-divider-theme="b"  data-inset="false">
                    <li data-role="list-divider" role="heading">
                        My Lists  <div style="padding-top:3px;float:right;font-size:9px;">(Logged in as <?=$username;?>. <a href="javascript:change('<?=$logoutUrl;?>');">Logout</a>)</div>
                    </li>
                    <?php 
                    if(count($lists) == 0){
                        echo '<div style="margin-bottom:10px;margin-top:10px;margin-left:16px;">No lists have been created. Click on the New List button to create a list.</div>';
                    } else {
                ?>
                <?php foreach($lists as $list): ?>
						<?php 
                            if($list['UserNotelist']['is_owner'] == 1){
                        ?>
                        <li data-theme="c">
							<a  href="<?=$viewItemUrl . '/'.$list['Notelist']['id'];?>" data-transition="slide">
								<?=$list['Notelist']['name'];?>
                                <?php if($list['Notelist']['user_id'] != $userId){ ?>
								<?php } ?>
                                <span class="ui-li-count">
									<?=$list['Notelist']['item_count'];?>
								</span>
							</a>
						</li>

                        <?php } 
                        
                        ?>
                    <?php endforeach; ?>
                    <?php }?>
                    <li data-role="list-divider" role="heading">
                        Shared Lists
                    </li>
                    <?php
                    if(count($shared) == 0){
                        echo '<div style="margin-top:10px;margin-left:16px;">No lists have been shared with you.</div>';
                    } else {
                        foreach($shared as $list){
                    ?>
                        <li data-theme="c">
							<a  href="<?=$viewItemUrl . '/'.$list['Notelist']['id'];?>" data-transition="slide">
								<?=$list['Notelist']['name'];?>
                                <?php if($list['Notelist']['user_id'] != $userId){ ?>
                                <div style="font-size:10px;">
                                <?php
                                    if($list['UserNotelist']['readonly'] == true){
                                        echo '(R) ';
                                    }
                                ?>
                                Shared with you by 
                                <span style="font-weight:bold !important;text-decoration:underline;"><?=$list['Owner'];?></span></div>
								<?php } ?>
                                <span class="ui-li-count">
									<?=$list['Notelist']['item_count'];?>
								</span>
							</a>
						</li>
                        <?php } 
                        }
                        ?>
                </ul>
            </div>
        </div>
