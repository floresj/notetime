
<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'index'
    ));
?>
<div data-role="page">
	<div data-theme="a" data-role="header" data-position="fixed">
		<a data-iconpos="notext" data-role="button" data-transition="slidedown" href="<?=$returnUrl;?>" data-icon="back" class="ui-btn-left"></a>
		<h3>
			New List
		</h3>
	</div>
	<div data-role="content" style="padding: 15px">
		<?php echo $this->Form->create('UserNotelist'); ?>
			<?php echo $this->Form->input('Notelist.name'); ?>
			<button type="submit" data-theme="b">Save</button>
		<?php echo  $this->Form->end(); ?>	
	</div>
</div>


