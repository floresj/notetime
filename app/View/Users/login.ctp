<?php
    $loginUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'index'
    ));
?>
<div data-role="page" id="page1" data-url="<?=$loginUrl;?>">
	<div data-theme="a" data-role="header" data-position="fixed">
		<h3>
			Note Time
		</h3>
	</div>
    <div data-role="content" style="padding: 15px">
        <div class="users form">
        <?php echo $this->Form->create('User'); ?>
            <fieldset>
            <?php
                echo $this->Form->input('username');
                echo $this->Form->input('password');
            ?>
            </fieldset>
        <?php

            $options = array(
                'label' => 'Login',
                'data-theme'=>'b'
            );		
            echo $this->Form->end($options); 
            if(isset($errorMsg)){
                echo $errorMsg;
            }
        ?>
        </div>
    </div>
</div>
<script>
</script>
