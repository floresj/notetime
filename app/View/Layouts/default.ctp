<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
        <title>
        <?php echo $title_for_layout; ?></title>
        <link rel="stylesheet" href="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.1/jquery.mobile-1.1.1.min.css" />
        <!--<link rel="stylesheet" href="my.css" />-->
        <style>
            /* App custom styles */
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">
        </script>
        <script src="https://ajax.aspnetcdn.com/ajax/jquery.mobile/1.1.1/jquery.mobile-1.1.1.min.js">
        </script>
        <!--<script src="my.js">-->
        <script>
            <?php
                $rootUrl = $this->Html->url('/');
                echo "var rootUrl ='$rootUrl'";
            ?>
        </script>
        <?php
			echo $this->fetch('meta');
			echo $this->fetch('css');
			echo $this->fetch('script');
		?>
    </head>
    <body>
		<?php echo $this->fetch('content'); ?>	
		<script src="<?=$this->webroot.'js/';?>notetime.js"></script>
        <?php
            $loginUrl = $this->Html->url(array(
                'controller'=> 'Users',
                'action'    => 'login'
            ));
        ?>
        <script>
            $( document ).bind( "pageloadfailed", function( event, data ){
                event.preventDefault();
                //data.deferred.resolve( data.absUrl, data.options, page );
                document.location.href = '<?=$loginUrl;?>';

            });
        </script>
    </body>
</html>
