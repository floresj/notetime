<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'view/' . $id
    ));
    $addUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'add/' . $id
    ));
?>
<div data-role="page" data-url="<?=$addUrl;?>">
	<div data-theme="a" data-role="header" data-position="fixed">
		<a data-iconpos="notext" data-role="button" data-transition="slidedown" href="<?=$returnUrl;?>" data-icon="back" class="ui-btn-left"></a>
		<h3>
			New Item
		</h3>
	</div>
	<div data-role="content" style="padding: 15px">
		<?=$this->Form->create('Item');?>		
		<?php
			echo $this->Form->textarea('name', array('style'=>'height:200px;'));
			//echo $this->Form->input('description', array('label'=>'Item Description'));
		?>
		<?php
			$options = array(
				'label' => 'Add',
				'data-theme'=>'a'
			);		
		?>
		<?=$this->Form->end($options);?>				
	</div>
</div>
