<?php
    $returnUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'index'
    ));
    $removeItemUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'remove'
    ));
    $addItemUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'add/' . $notelistId
    ));
    $viewItemUrl = $this->Html->url(array(
        'controller'=> 'items',
        'action'    => 'view/' . $notelistId
    ));
    $editListUrl = $this->Html->url(array(
        'controller'=> 'notelists',
        'action'    => 'edit/' . $notelistId
    ));    
    $shareListUrl = $this->Html->url(array(
        'controller'=> 'UserNotelists',
        'action'    => 'share',
         $notelistId
    ));    
?>
<style>
.ui-grid-b > *:nth-child(n) {
    margin-right: 0px;
    width: 33.333%;
}
</style>
<div id="viewitems" data-role="page" data-url="<?=$viewItemUrl;?>">
<script>
    $('.viewitem-row').live('swipeleft', function(event){
            var id = $(this).attr('itemid');
            var row = $(this).attr('itemcount');
            Items.remove(id, row);
    });
	function change(url){
		$.mobile.changePage(
				url,
				{
				  transition              : 'fade',
				  showLoadMsg             : true,
				  reloadPage              : true
				}
			  );	
	}
    
</script>
    <div data-theme="a" data-role="header" data-position="fixed">
        <a data-iconpos="notext" data-role="button" data-transition="slide" href="javascript:change('<?=$returnUrl;?>');" data-icon="back" class="ui-btn-left"></a>			
        <h3>
            <?=$notelistName;?>
        </h3>
        <?php if($isReadOnly == false):?>
        <a data-iconpos="notext"  data-role="button" data-transition="slide" href="<?=$addItemUrl;?>" data-icon="plus" class="ui-btn-right"></a>
        <?php endif?>
    </div>
    <div data-role="navbar">
        <ul>
            <?php if($isOwner){ ?>
            <li><a href="<?=$editListUrl;?>" data-theme="b">Edit</a></li>
            <li><a href="javascript:change('<?=$shareListUrl;?>');" data-theme="b">Share</a></li>
            <?php }?>
            <li style="margin-right:0px !important;"><a href="javascript:Items.sync();" data-theme="b">Synchronize</a></li>
        </ul>
    </div>
    <div data-role="content" style="padding: 15px">
	<div id="listview-container">
       <ul id="items-listview" data-role="listview" data-icon="check" data-inset="false" data-split-theme="d">
            <?php
                $liCount = 0;
            ?>
            <?php foreach($items as $item): ?>
            <?php
                // If List is NOT readonly, then display list with links
                if($isReadOnly == false){
            ?>
            <li itemcount="<?=$liCount;?>" data-icon="check" itemid="<?=$item['Item']['id'];?>" class="viewitem-row" data-theme="c" id="item<?=$liCount;?>">
                <a href="javascript:;">
                    <h3 class="ui-li-heading"><?=$item['Item']['name'];?></h3>
                    <?php if($isReadOnly == false): ?>
                    <a href="javascript:Items.remove(<?=$item['Item']['id'];?>, <?=$liCount;?>);" itemid="<?=$item['Item']['id'];?>" data-shadow="false" data-iconshadow="true" data-icon="false" data-iconpos="notext" data-theme="a">						
                    <?php endif?>    
                </a>
            </li>
            <?php
            //  If list IS readonly, then display in readonly mode without links
            } else {
            ?>

            <li itemcount="<?=$liCount;?>" itemid="<?=$item['Item']['id'];?>" class="viewitem-row" data-theme="c" id="item<?=$liCount;?>">
                    <h3 class="ui-li-heading"><?=$item['Item']['name'];?></h3>
            </li>
            <?php
            }
            ?>
            <?php
                $liCount++;
            ?>
            <?php endforeach?>
        </ul>
        </div>
    </div>
</div>
